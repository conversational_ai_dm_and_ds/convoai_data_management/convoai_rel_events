# Sample GitLab Project

This project contains the scripts for loading and maintianing the conversational AI release events table.

Note:

1. Conversational AI releases should be updated within the spreadsheet after each release.
2. All updates will be loaded within a excel or CSV file and load into the DM repo.
3. The python script will run on a set schedule to refresh the table in Nucleus 

